FROM php:7-fpm-alpine

ENV WWW_DIR="/app"

WORKDIR ${WWW_DIR}

COPY . .
