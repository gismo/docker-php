ARGS                   = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS              += --silent
.PHONY: test documentation
DOCKER_REPOSITORY      = `cat DOCKER_REPOSITORY`
DOCKER_TAG_LATEST      = `cat DOCKER_TAG_LATEST`
PAPER                  =
BUILDDIR               = _build
PAPEROPT_a4            = -D latex_paper_size=a4
PAPEROPT_letter        = -D latex_paper_size=letter
ALLSPHINXOPTS          = -d $(BUILDDIR)/doctrees $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) .


VERSION_COMMIT_HASH    = $$(git rev-parse HEAD)
VERSION_CURRENT_BRANCH = $$(git rev-parse --abbrev-ref HEAD)
VERSION_BUILD_DATE     = "$$(date)"
VERSION_BUILD_DATE1    =$(date)
FONT_MAGENTA           =\e[35m
FONT_RED               =\e[41m
FONT_DEFAULT           =\e[0m
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-$(HELP_SPACE)s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/| sort'

install: ## build l'image nginx
	docker-compose build --no-cache --compress --parallel nginx

run: install ## Execute la stack et attends les connexions
	docker-compose up

test: ## Lance un test
	docker pull $CONTAINER_RELEASE_IMAGE
	docker run -itd -p 9000:9000 --name test-fpm $CONTAINER_RELEASE_IMAGE
	docker run -itd -p 80:80 --link test-fpm --name test-nginx -v ``pwd``./default.conf:/etc/nginx/conf.d/default.conf nginx:1.17-alpine
	docker exec test-nginx sh -c "pwd ; ls -lha /app"
